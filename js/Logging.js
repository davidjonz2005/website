﻿$(function () {
    // Ajax to Login :
    $(".Kbh_login_btn_clickable").click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/WebSiteBuilder/SignIn",
            data: "UserName=" + $("#Kbh_login_UserName").val() + "&Password=" + $("#Kbh_login_Password").val() + "&__RequestVerificationToken=" + $("input[name='__RequestVerificationToken']").val(),
            dataType: "json",
            success: function (response) {
                if (response.result == "true") {
                    window.location.reload(true);
                }
                else {
                    $(".Kbh_result_error").html(response.message);
                }
            }
        });
    });
});